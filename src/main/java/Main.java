import akka.actor.*;
import com.typesafe.config.ConfigFactory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static HashMap<String, ActorSystem> systemsDict;
    public static HashMap<String, ActorRef> actorsDict;
    public static int nbMappers;
    public static int nbReducers;

    public static void main(String[] args) {

        if(args.length != 2){
            System.out.println("The required arguments were not found. Required arguments are the number of mappers and the number of reducers");
            return;
        }
        try{
            int nbEntryMappers = Integer.parseInt(args[0]);
            int nbEntryReducers = Integer.parseInt(args[1]);
            if(nbEntryMappers <= 0){
                nbEntryMappers = 1;
                System.out.println("Number of mappers too low. Running with 1 mapper.");
            }
            if(nbEntryReducers <= 0) {
                nbEntryReducers = 1;
                System.out.println("Number of reducers too low. Running with 1 reducer.");
            }
            else if(nbEntryReducers > 26){
                nbEntryReducers = 26;
                System.out.println("Number of reducers too high. Running with 26 reducers.");
            }
            nbMappers = nbEntryMappers;
            nbReducers = nbEntryReducers;
        }
        catch (Exception e){
            System.out.println("Couldn't parse number of mappers and reducers. Please check that you gave integer numbers.");
            return;
        }

        initSystems();
        initActors();

        InputStream stream = Main.class.getClassLoader().getResourceAsStream("Text.txt");
        getActor("master").tell(stream, ActorRef.noSender());

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();     //Waiting for user input to print the results of both reducers
        callReducers();
    }

    private static void initSystems(){

        HashMap<String, ActorSystem> tempSystemsDict = new HashMap<>();

        ActorSystem mappersSystem = ActorSystem.create("mappersSystem", ConfigFactory.load("applicationMappers"));      //2552
        ActorSystem reducersSystem = ActorSystem.create("reducersSystem", ConfigFactory.load("applicationReducers"));   //2553

        tempSystemsDict.put("mappersSystem", mappersSystem);
        tempSystemsDict.put("reducersSystem", reducersSystem);

        systemsDict = tempSystemsDict;
    }

    private static void initActors(){

        HashMap<String, ActorRef> map = new HashMap<>();
        ActorSystem mappersSystem = systemsDict.get("mappersSystem");
        ActorSystem reducersSystem = systemsDict.get("reducersSystem");

        ActorRef master = mappersSystem.actorOf(Props.create(Master.class), "master");
        map.put("master", master);

        for(int i = 1; i <= nbMappers; i++){
            String currentMapper = "mapper" + i;
            map.put(currentMapper, mappersSystem.actorOf(Props.create(MapperActor.class), currentMapper));
        }
        for(int j = 1; j <= nbReducers; j++){
            String currentReducer = "reducer" + j;
            map.put(currentReducer, reducersSystem.actorOf(Props.create(ReducerActor.class), currentReducer));
        }
        actorsDict = map;

        System.out.println(getActor("mapper1").path().name() + " is on " + mappersSystem.provider().getDefaultAddress());
        System.out.println(getActor("reducer1").path().name() + " is on " + reducersSystem.provider().getDefaultAddress());
    }

    private static void callReducers() {
        for(Map.Entry<String, ActorRef>entry: actorsDict.entrySet()){
            if(entry.getKey().contains("reducer")){
                getActor(entry.getKey()).tell(true, ActorRef.noSender());
            }
        }
    }

    public static ActorRef getActor(String actorName){
        return actorsDict.get(actorName);
    }

}
