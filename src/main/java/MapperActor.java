import akka.actor.ActorRef;
import akka.actor.UntypedActor;

import java.text.Normalizer;
import java.util.Locale;

public class MapperActor extends UntypedActor {

    @Override
    public void onReceive(Object message) {
        if(message instanceof String){
            String[] words = ((String) message).replaceAll("^[.,?!'\":;\\s]+", "").split("[.,?!'\":;\\s]+");
            for (String word: words){
                String reducer = partition(word);
                Main.actorsDict.get(reducer).tell(word, ActorRef.noSender());
            }
        }
        else{
            unhandled(message);
        }
    }

    private String partition(String word){
        return "reducer" + getRelatedReducer(word);
    }

    private int getRelatedReducer(String word){
        String wordWithoutAccents = Normalizer.normalize(word, Normalizer.Form.NFD);
        String tempWord = wordWithoutAccents.toLowerCase(Locale.ROOT);
        char currentChar = tempWord.charAt(0);
        int charIndex = currentChar - 96;
        int currentReducer = 1;
        int separation = 26/Main.nbReducers;
        for(int i = separation; i <= 26; i = i + separation){
            if(charIndex <= i)
                return currentReducer;
            else{
                if(currentReducer == Main.nbReducers){
                    if(isNotFull())     //The division doesn't give an int number, but a double, meaning that the last bloc has to contain the remaining letters
                        return currentReducer;
                }
                currentReducer++;
            }
        }
        return currentReducer;
    }

    private boolean isNotFull(){
        double doubleValue = (26.0/(float)Main.nbReducers);
        return doubleValue != (int) doubleValue;
    }

}
