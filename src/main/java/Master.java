import akka.actor.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Master extends UntypedActor{

    @Override
    public void onReceive(Object message) {
        if(message instanceof InputStream){
            distributeFileLines((InputStream) message);
        }
        else
            unhandled(message);
    }

    private void distributeFileLines(InputStream stream){
        int currentMapper = 1;
        String currentMapperName;

        if(stream != null){
            InputStreamReader inputReader = new InputStreamReader(stream);
            BufferedReader reader = new BufferedReader(inputReader);
            try{
                String line;
                while((line = reader.readLine()) != null){
                    currentMapperName = "mapper" + currentMapper;
                    Main.getActor(currentMapperName).tell(line, ActorRef.noSender());
                    if(currentMapper == Main.nbMappers)
                        currentMapper = 1;
                    else
                        currentMapper++;
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
