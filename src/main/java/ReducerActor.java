import akka.actor.UntypedActor;

import java.util.HashMap;
import java.util.Map;

public class ReducerActor extends UntypedActor {

    private final HashMap<String, Integer> wordsList = new HashMap<>();

    @Override
    public void onReceive(Object message) {
        if(message instanceof String){
            String strMessage = (String) message;
            if(wordsList.containsKey(strMessage)){
                int currentNumber = wordsList.get(strMessage) + 1;
                wordsList.put(strMessage, currentNumber);

            }else
                wordsList.put(strMessage, 1);
            System.out.println(strMessage + " -> " + wordsList.get(strMessage));
        }
        else if(message instanceof Boolean){
            printWords();
        }
        else{
            unhandled(message);
        }
    }

    private void printWords() {
        int size = wordsList.size();
        int count = 0;
        for(Map.Entry<String, Integer>entry: wordsList.entrySet()){
            System.out.println(self().path().name() + ": mot " + entry.getKey() + ": " + entry.getValue());
            count += entry.getValue();
        }
        System.out.println(self().path().name() + ": total de " + size + " mots avec " + count + " occurences.");
    }

}
